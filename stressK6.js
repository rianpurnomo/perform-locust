import http from 'k6/http';
import { sleep } from 'k6';

export const options = {
    stages: [
        { duration: '20s', target: 200 }, 
        { duration: '60s', target: 200 }, 
        { duration: '70s', target: 200 }, 
        { duration: '10s', target: 200 }, 
        { duration: '2s', target: 0 }, // ramp-down to 0 users
    ],
};

export default () => {
    http.get('https://www.asia-quest.jp/en//service/solution/consulting');
    sleep(1);
};

