from locust import HttpUser, between, task

class aqiWeb(HttpUser):
    host = 'https://www.asia-quest.jp/en'
    wait_time = between(5, 15)
    @task
    def get_homePage(self):
        self.client.get('/')
        self.client.get('/service/solution/consulting')