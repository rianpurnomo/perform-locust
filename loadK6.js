import http from 'k6/http';
import { sleep } from 'k6';

export const options = {
    stages: [
        { duration: '2s', target: 100 },
        { duration: '100s', target: 100 }, 
        { duration: '5s', target: 100 },  // ramp-down to 0 users
    ],
};

export default () => {
    http.get('https://www.asia-quest.jp/en/service/solution/consulting');
    sleep(1);
};
